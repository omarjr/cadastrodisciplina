import java.util.ArrayList;

public class ControleAluno {

// atributos
	public ArrayList<Aluno> listaAlunos;
	

// construtor
	public ControleAluno() {
		listaAlunos = new ArrayList<Aluno>();
	}

// métodos
	public String adicionar(Aluno umAluno) {
		listaAlunos.add(umAluno);
		String mensagem = "Aluno matriculado com sucesso!";
		return mensagem;
	}

	public String remover(Aluno umAluno) {
		listaAlunos.remove(umAluno);
		String mensagem = "Aluno removido com sucesso!";
		return mensagem;
	}

	public Aluno pesquisar(String umNome){
		for(Aluno umAluno: listaAlunos){
			if(umAluno.getNome().equalsIgnoreCase(umNome)) return umAluno;
		}
		return null;
	}

}
