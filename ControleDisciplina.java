import java.util.ArrayList;

public class ControleDisciplina {

// atributos
	public ArrayList<Disciplina> listaDisciplinas;
	

// construtor
	public ControleDisciplina() {
		listaDisciplinas = new ArrayList<Disciplina>();
	}

// métodos
	public String adicionar(Disciplina umaDisciplina){
		listaDisciplinas.add(umaDisciplina);
		String mensagem = "Disciplina adicionado com sucesso!";
		return mensagem;
	}

	public String remover(Disciplina umaDisciplina) {
		listaDisciplinas.remove(umaDisciplina);
		String mensagem = "Disciplina removida com sucesso!";
		return mensagem;
	}

	public Disciplina pesquisar(String umNomeDisciplina){
		for(Disciplina umaDisciplina: listaDisciplinas){
			if(umaDisciplina.getNomeDisciplina().equalsIgnoreCase(umNomeDisciplina)) return umaDisciplina;
		}
		return null;
	}	

}

