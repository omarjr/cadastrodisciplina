import java.util.ArrayList;

public class Disciplina{

// atributos

	public String nomeDisciplina;
	public String codigo;
	public ArrayList<Aluno> listaAlunos;

// construtor

	public Disciplina(){
		listaAlunos = new ArrayList<Aluno>();
	}

// métodos

		public void setNomeDisciplina(String umNomeDisciplina) {
			nomeDisciplina = umNomeDisciplina;	
		}


		public String getNomeDisciplina() {
			return nomeDisciplina;
		}
		
		public void setCodigo(String umCodigo){
			codigo = umCodigo;
		}		


		public String getCodigo(){
			return codigo;
		}

		public String adicionarAluno(Aluno umAluno){
			String mensagem = "O aluno foi matriculado na disciplina com sucesso!!";
			listaAlunos.add(umAluno);
			return mensagem;
		}

		public String removerAluno(Aluno umaAluna){
			String mensagem = "O aluno foi removido da disciplina com sucesso!!";
			listaAlunos.remove(umaAluna);
			return mensagem;
		}

		public Aluno pesquisarAluno(String umNome){
			for(Aluno alunoPesquisado: listaAlunos){
				if(alunoPesquisado.getNome().equalsIgnoreCase(umNome)){
					return alunoPesquisado;
				}
			}
			return null;
		}


}
