import java.util.Scanner;
import java.io.*;

public class CadastroAlunoDisciplina{

	public static void main(String[] args) throws IOException{
	InputStream entradaSistema = System.in;
	InputStreamReader leitor = new InputStreamReader (entradaSistema);
	BufferedReader leitorEntrada = new BufferedReader(leitor);
	String entradaTeclado;


	// istanciando objetos do sistema
	ControleAluno umControleAluno = new ControleAluno();
	Aluno umAluno = new Aluno();
	ControleDisciplina umControleDisciplina = new ControleDisciplina();
	Disciplina umaDisciplina = new Disciplina();


//criando o menu

		boolean continuar = true; //variável de loop

		int opcao;

		Scanner entrada = new Scanner(System.in);

		while(continuar){
			System.out.println("\n<<<<<<	Menu	>>>>>>\n");
			System.out.println("1 - Adicionar disciplina");
			System.out.println("2 - Remover disciplina");
			System.out.println("3 - Pesquisar disciplina");
			System.out.println("4 - Adicionar aluno");
			System.out.println("5 - Remover aluno");
			System.out.println("6 - Pesquisar aluno");			
			System.out.println("7 - Adicionar aluno em uma disciplina");
			System.out.println("8 - Remover aluno de uma disciplina");
			System.out.println("9 - Pesquisar aluno em uma disciplina");
			System.out.println("0 - Concluir cadastro\n");
			System.out.println("Digite o número da opção desejada [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]");
			opcao = entrada.nextInt();

			switch(opcao){
				case 1:
					System.out.println("Digite o nome da disciplina: ");
					entradaTeclado = leitorEntrada.readLine();
					String umNomeDisciplina = entradaTeclado;
					umaDisciplina.setNomeDisciplina(umNomeDisciplina);

					System.out.println("Digite o código da disciplina: ");
					entradaTeclado = leitorEntrada.readLine();
					String umCodigo = entradaTeclado;
					umaDisciplina.setCodigo(umCodigo);

    				//adicionando uma disciplina na lista de disciplinas do sistema
    		    			String mensagem = umControleDisciplina.adicionar(umaDisciplina);

				//conferindo saída
    					System.out.println("\n=================================");
    		    			System.out.println(mensagem);
					System.out.println("=)");
					System.out.println("\n");
					break;


				case 2:

				    	System.out.println("Digite o nome da disciplina que deseja remover:");
					entradaTeclado = leitorEntrada.readLine();
					Disciplina disciplinaPesquisar = umControleDisciplina.pesquisar(entradaTeclado);
				//removendo uma pessoa da lista de pessoas do sistema
					String mensagem2 = umControleDisciplina.remover(umaDisciplina);


				//conferindo saída
					System.out.println("\n=================================");
					System.out.println(mensagem2);
					System.out.println("=)");
					System.out.println("\n");
					break;


				case 3:

		    		//Pesquisando uma disciplina na lista de disciplinas do sistema
					System.out.println("Digite o nome da disciplina que deseja pesquisar:");
					entradaTeclado = leitorEntrada.readLine();
					String nomeDisciplinaPesquisar = entradaTeclado;
					Disciplina mensagem3 = umControleDisciplina.pesquisar(nomeDisciplinaPesquisar);

				//conferindo saída
					System.out.println("\nDisciplina: " + mensagem3.getNomeDisciplina() + "\nCódigo: " + mensagem3.getCodigo() + "\n");
					break;


				case 4:

					System.out.println("Digite o nome do aluno: ");
					entradaTeclado = leitorEntrada.readLine();
					String umNome = entradaTeclado;
					umAluno.setNome(umNome);

					System.out.println("Digite a matrícula do aluno: ");
					entradaTeclado = leitorEntrada.readLine();
					String umaMatricula = entradaTeclado;
					umAluno.setMatricula(umaMatricula);

    				//adicionando aluno na lista de alunos do sistema
    		    			String mensagem4 = umControleAluno.adicionar(umAluno);

				//conferindo saída
    					System.out.println("\n=================================");
    		    			System.out.println(mensagem4);
					System.out.println("=)");
					System.out.println("\n");
					break;

				case 5:

				//removendo uma pessoa da lista de pessoas do sistema
				    	System.out.println("Digite o nome do aluno que deseja remover:");
					entradaTeclado = leitorEntrada.readLine();
					String nomeRemover = entradaTeclado;
					Aluno alunoRemover = umControleAluno.pesquisar(nomeRemover);

				//removendo aluno da lista de alunos do sistema
					String mensagem5 = umControleAluno.remover(alunoRemover);

				//conferindo saída
					System.out.println("\n=================================");
					System.out.println(mensagem5);
					System.out.println("=)");
					System.out.println("\n");
					break;


				case 6:

					System.out.println("Digite o nome do aluno que deseja pesquisar:");
					entradaTeclado = leitorEntrada.readLine();
					String nomePesquisar = entradaTeclado;

		    		//pesquisando um aluno na lista de alunos do sistema
					Aluno mensagem6 = umControleAluno.pesquisar(nomePesquisar);

				//conferindo saída
					System.out.println("\nNome: " + mensagem6.getNome() + "\nMatrícula: " + mensagem6.getMatricula() + "\n");
					break;


				case 7:
					System.out.println("Digite o nome da disciplina: ");
					entradaTeclado = leitorEntrada.readLine();
					Disciplina DisciplinaPesquisada = umControleDisciplina.pesquisar(entradaTeclado);
					System.out.println("Digite o nome do aluno que deseja adicionar: ");
					entradaTeclado = leitorEntrada.readLine();
					Aluno aluno = umControleAluno.pesquisar(entradaTeclado);
					
				//adicionando aluno em disciplina
					String mensagem7 = DisciplinaPesquisada.adicionarAluno(aluno);
						
				//conferindo saída
					System.out.println("\n=================================");
    					System.out.println(mensagem7);
					System.out.println("=)");
					System.out.println("\n");
					break;


				case 8:

					System.out.println("Digite o nome da disciplina: ");
					entradaTeclado = leitorEntrada.readLine();
					Disciplina DisciplinaPesquisada2 = umControleDisciplina.pesquisar(entradaTeclado);
					System.out.println("Digite o nome do aluno que deseja remover: ");
					entradaTeclado = leitorEntrada.readLine();
					Aluno AlunoRemover = DisciplinaPesquisada2.pesquisarAluno(entradaTeclado);
						
				//removendo aluno da disciplina
					String mensagem8 = DisciplinaPesquisada2.removerAluno(AlunoRemover);
					
				//conferindo saída
					System.out.println("\n=================================");
    					System.out.println(mensagem8);
					System.out.println("\n");
					break;

				case 9:

					System.out.println("Digite o nome da disciplina: ");
					entradaTeclado = leitorEntrada.readLine();
					Disciplina DisciplinaPesquisada3 = umControleDisciplina.pesquisar(entradaTeclado);
					System.out.println("\n");
					System.out.println("Digite o nome do aluno que deseja pesquisar: ");
					entradaTeclado = leitorEntrada.readLine();

				//pesquisando aluno na lista de disciplinas do sistema
					Aluno mensagem9 = DisciplinaPesquisada3.pesquisarAluno(entradaTeclado);
					
					//conferindo saída
					System.out.println("\n=================================");
				        System.out.println("Aluno regularmente matriculado na discipina!!!");
					System.out.println("Nome: " + mensagem9.getNome() + " Matrícula: " + mensagem9.getMatricula() + "."); 
					break;

				case 0:

				        System.out.println("\nCadastro concluído!\n");
					continuar=false;
					break;

				default:
					System.out.println("\nOpção inválida!\n");

			}
		
		}

	}

}

